import oscP5.*;
import netP5.*;

import themidibus.*; //Import the library
import javax.sound.midi.MidiMessage; //Import the MidiMessage classes http://java.sun.com/j2se/1.5.0/docs/api/javax/sound/midi/MidiMessage.html
import javax.sound.midi.SysexMessage;
import javax.sound.midi.ShortMessage;

MidiBus midiProPresenter;
MidiBus midiJands;

OscP5 oscP5;
NetAddress oscATEM;
OscMessage oscKey1on;
OscMessage oscKey1off;
OscMessage oscKey2on;
OscMessage oscKey2off;

final static int anPP5 = 511;
final static int anATEM = 510;
final static int chPP5 = 1;

boolean bWantKey1 = false;
boolean bWantKey2 = false;

boolean bDisableKey1 = false;
boolean bDisableKey2 = false;

int iATEMProgram = 0;

ArtNetListener artNetListener;
byte[] inputDmxArray;
// Previous values for ATEM & PP5 channels
int lvATEM = 0;
int lvPP5 = 0;

void setup() {
  size( 600, 100);
  if( frame != null) { frame.setResizable( true); }
  textSize( 16);

  println( "Starting ...");

  MidiBus.list(); // List all available Midi devices on STDOUT. This will show each device's index and name.
  midiProPresenter = new MidiBus(this, "ProPresenter", "ProPresenter"); // Create a new MidiBus object

  // On mac you will need to use MMJ since Apple's MIDI subsystem doesn't properly support SysEx. 
  // However MMJ doesn't support sending timestamps so you have to turn off timestamps.
  // midiProPresenter.sendTimestamps(false);

  /* start oscP5, listening for incoming messages at port 4444 */
  oscP5 = new OscP5(this,4444);
  
  /* myRemoteLocation is a NetAddress. a NetAddress takes 2 parameters,
   * an ip address and a port number. myRemoteLocation is used as parameter in
   * oscP5.send() when sending osc packets to another computer, device, 
   * application. usage see below. for testing purposes the listening port
   * and the port of the remote location address are the same, hence you will
   * send messages back to this sketch.
   */
  oscATEM = new NetAddress("172.16.0.201",3333);
  oscKey1on = new OscMessage("/atem/usk/1");
  oscKey1on.add(1);
  oscKey1off = new OscMessage("/atem/usk/1");
  oscKey1off.add(0);
  oscKey2on = new OscMessage("/atem/usk/2");
  oscKey2on.add(1);
  oscKey2off = new OscMessage("/atem/usk/2");
  oscKey2off.add(0);
  
  // Artnet Listener
  artNetListener = new ArtNetListener();
}

void exit() {
  println( "Exiting ...");
  midiProPresenter.close();
  artNetListener.stopArtNet();
  super.exit();
}

void setKeyer(int k, boolean v) {
  if(k == 1) {
    bWantKey1 = v;
    if((v == true) && (bDisableKey1 == false)) {
      oscP5.send(oscKey1on, oscATEM);
    } else {
      oscP5.send(oscKey1off, oscATEM);
    }
  }
  if(k == 2) {
    bWantKey2 = v;
    if((v == true) && (bDisableKey2 == false)) {
      oscP5.send(oscKey2on, oscATEM);
    } else {
      oscP5.send(oscKey2off, oscATEM);
    }
  }
}

void draw() {
  int nvPP5, nvATEM;
  inputDmxArray = artNetListener.getCurrentInputDmxArray(); 
  nvPP5 = artNetListener.toInt(inputDmxArray[anPP5]);
  nvATEM = artNetListener.toInt(inputDmxArray[anATEM]);

  background( 0);
  fill( 255);
  text( "PP5 DMX: " + nvPP5, 20, 20);
  text( "ATEM DMX: " + nvATEM, 300, 20);

  switch(nvPP5/16) {
    case 1:
      text( "Pre/Post", 20, 40);
      break;
    case 2:    case 3:
      text( "Song 1, Cue " + str( int( (nvPP5 % 32) + 1 ) ) , 20, 40);
      break;
    case 4:    case 5:
      text( "Song 2, Cue " + str( int( (nvPP5 % 32) + 1 ) ) , 20, 40);
      break;
    case 6:    case 7:
      text( "Welcome", 20, 40);
      break;
    case 8:    case 9:
      text( "Song 3, Cue " + str( int( (nvPP5 % 32) + 1 ) ) , 20, 40);
      break;
    case 10:
      text( "Roll-In", 20, 40);
      break;
    case 11:
      text( "Sermon", 20, 40);
      break;
    case 12:   case 13:
      text( "Song 4, Cue " + str( int( (nvPP5 % 32) + 1 ) ) , 20, 40);
      break;
  };

  switch(nvATEM/16) {
    case 1:
      text( "Pre/Post", 300, 40);
      break;
    case 2:    case 3:
      text( "Song 1, Cue " + str( int( (nvATEM % 32) + 1 ) ) , 300, 40);
      break;
    case 4:    case 5:
      text( "Song 2, Cue " + str( int( (nvATEM % 32) + 1 ) ) , 300, 40);
      break;
    case 6:    case 7:
      text( "Welcome", 300, 40);
      break;
    case 8:    case 9:
      text( "Song 3, Cue " + str( int( (nvATEM % 32) + 1 ) ) , 300, 40);
      break;
    case 10:
      text( "Roll-In", 300, 40);
      break;
    case 11:
      text( "Sermon", 300, 40);
      break;
    case 12:   case 13:
      text( "Song 4, Cue " + str( int( (nvATEM % 32) + 1 ) ) , 300, 40);
      break;
  };

  switch(nvATEM/16) {
    default:
    case 1: // Pre/Post
    case 6: case 7: // Welcome
    case 10: // Roll-In
      text( "Key1 Off", 300, 60);
      text( "Key2 Off", 300, 80);
      break;
    case 11: // Sermon
      text( "Key1 On", 300, 60);
      text( "Key2 Off", 300, 80);
      break;
    case 2:    case 3: // Music
    case 4:    case 5:
    case 8:    case 9:
    case 12:    case 13:
        text( "Key1 Off", 300, 60);
        text( "Key2 On", 300, 80);
      break;
  };

  switch(iATEMProgram) {
    case 1: text( "Kidstown Numbers", 300, 100); break;
    case 2: text( "Camera 1", 300, 100); break;
    case 3: text( "Camera 2", 300, 100); break;
    case 4: text( "Camera 3", 300, 100); break;
    case 5: text( "Camera 4", 300, 100); break;
    case 6: text( "Camera 5", 300, 100); break;
    case 7: text( "Camera 6", 300, 100); break;
    case 8: text( "Sermon Notes", 300, 100); break;
    case 10: text( "Front of House", 300, 100); break;
    case 12: text( "Lighting Backgrounds", 300, 100); break;
    default: text( "Unknown Input", 300, 100); break;
  };
  
  if(lvATEM != nvATEM) {
    lvATEM = nvATEM;

    switch(nvATEM/16) {
      default:
      case 1: // Pre/Post
      case 6: case 7: // Welcome
      case 10: // Roll-In
        setKeyer(1, false);
        setKeyer(2, false);
        break;
      case 11: // Sermon
        setKeyer(1, true);
        setKeyer(2, false);
        break;
      case 2:    case 3: // Music
      case 4:    case 5:
      case 8:    case 9:
      case 12:    case 13:
        setKeyer(1, false);
        setKeyer(2, true);
	break;
    }
  }

  if(lvPP5 != nvPP5) {
    lvPP5 = nvPP5;

    /* Blackout */
    if (nvPP5 == 0) {
      text( "Clear All", 20, 40);
      // Midi 1 : Clear All
      midiProPresenter.sendNoteOn(chPP5, 1, 1);
      midiProPresenter.sendNoteOff(chPP5, 1, 1);
    }

    /* Harvest Cues 1.0-1.x */ 
    if ((nvPP5 >= 16 && nvPP5 < 32)) {
        // Midi 19 : Select Playlist  V: 1
        midiProPresenter.sendNoteOn(chPP5, 19, (nvPP5 / 16));
        midiProPresenter.sendNoteOff(chPP5, 19, (nvPP5 / 16));

        // Midi 20 : Trigger Slide  V: 1
        midiProPresenter.sendNoteOn(chPP5, 20, (nvPP5 & 0xf)+1);
        midiProPresenter.sendNoteOff(chPP5, 20, (nvPP5 & 0xf)+1);
    }
    
    /* Harvest Cues 2.0-3.x, 4.0-5.x, 8.0-9.x, 12.0-13.x */ 
    if ((nvPP5 >= 32 && nvPP5 < 64) ||
        (nvPP5 >= 64 && nvPP5 < 96) ||
        (nvPP5 >= 128 && nvPP5 < 160) ||
        (nvPP5 >= 192 && nvPP5 < 224)
       ) {
      // Midi 19 : Select Playlist  V: 1
      midiProPresenter.sendNoteOn(chPP5, 19, (nvPP5 / 32)+1);
      midiProPresenter.sendNoteOff(chPP5, 19, (nvPP5 / 32)+1);
      // Midi 20 : Trigger Slide  V: 1
      midiProPresenter.sendNoteOn(chPP5, 20, (nvPP5 & 0x1f)+1);
      midiProPresenter.sendNoteOff(chPP5, 20, (nvPP5 & 0x1f)+1);
    }

    /* Harvest Cues 6.0-7.x, 10.0-11.x */ 
    if ((nvPP5 >= 96 && nvPP5 < 128) ||
        (nvPP5 >= 160 && nvPP5 < 176) ||
        (nvPP5 >= 176 && nvPP5 < 191)
       ) {
      // Midi 19 : Select Playlist  V: 1
      midiProPresenter.sendNoteOn(chPP5, 19, (nvPP5 / 32)+1);
      midiProPresenter.sendNoteOff(chPP5, 19, (nvPP5 / 32)+1);

      // Midi 20 : Trigger Slide  V: 1
      midiProPresenter.sendNoteOn(chPP5, 20, 1);
      midiProPresenter.sendNoteOff(chPP5, 20, 1);
    }

  }

  displayStatus();
}

void displayStatus() {
  fill( 255);
  text( inputDmxArray.length + " DMX channels, " + str( int( frameRate)) + " fps"
        , 20, 80);
}

void oscEvent(OscMessage theOscMessage) {
  // Kidstown Numbers - Should never be selected.
  if(theOscMessage.checkAddrPattern("/atem/program/1") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 1;
    }
  }
  // Cameras 1 - 6 - Enable Keys
  if(theOscMessage.checkAddrPattern("/atem/program/2") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 2;
      bDisableKey1 = false;
      bDisableKey2 = false;
      setKeyer(2, bWantKey2);
    }
  }
  // Cameras 1 - 6 - Enable Keys
  if(theOscMessage.checkAddrPattern("/atem/program/3") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 3;
      bDisableKey1 = false;
      bDisableKey2 = false;
      setKeyer(2, bWantKey2);
    }
  }
  // Cameras 1 - 6 - Enable Keys
  if(theOscMessage.checkAddrPattern("/atem/program/4") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 4;
      bDisableKey1 = false;
      bDisableKey2 = false;
      setKeyer(2, bWantKey2);
    }
  }
  // Cameras 1 - 6 - Enable Keys
  if(theOscMessage.checkAddrPattern("/atem/program/5") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 5;
      bDisableKey1 = false;
      bDisableKey2 = false;
      setKeyer(2, bWantKey2);
    }
  }
  // Cameras 1 - 6 - Enable Keys
  if(theOscMessage.checkAddrPattern("/atem/program/6") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 6;
      bDisableKey1 = false;
      bDisableKey2 = false;
      setKeyer(2, bWantKey2);
    }
  }
  // Cameras 1 - 6 - Enable Keys
  if(theOscMessage.checkAddrPattern("/atem/program/7") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 7;
      bDisableKey1 = false;
      bDisableKey2 = false;
      setKeyer(2, bWantKey2);
    }
  }
  // Sermon Notes - Disable Key1 & Key2 immediately (Lobby Loop)
  if(theOscMessage.checkAddrPattern("/atem/program/8") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 8;
      bDisableKey1 = true;
      bDisableKey2 = true;
      setKeyer(1, false);
      setKeyer(2, false);
    }
  }
  // Unused input
  if(theOscMessage.checkAddrPattern("/atem/program/9") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 9;
    }
  }
  // Front of House - Disable Key2 immediately
  if(theOscMessage.checkAddrPattern("/atem/program/10") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 10;
      bDisableKey2 = true;
      setKeyer(2, false);
    }
  }
  // Unused input
  if(theOscMessage.checkAddrPattern("/atem/program/11") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 11;
    }
  }
  // Lighting Background
  if(theOscMessage.checkAddrPattern("/atem/program/12") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 12;
      bDisableKey1 = false;
      bDisableKey2 = false;
    }
  }
  // Unused input
  if(theOscMessage.checkAddrPattern("/atem/program/13") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 13;
    }
  }
  // Unused input
  if(theOscMessage.checkAddrPattern("/atem/program/14") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 14;
    }
  }
  // Unused input
  if(theOscMessage.checkAddrPattern("/atem/program/15") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 15;
    }
  }
  // Unused input
  if(theOscMessage.checkAddrPattern("/atem/program/16") == true) {
    if(theOscMessage.get(0).floatValue() == 1.0) {
      iATEMProgram = 16;
    }
  }
}

