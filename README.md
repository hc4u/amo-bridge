# ArtNet to MIDI & OSC Bridge

A Processing sketch that listens to ArtNet DMX Input packets and controls ProPresenter6 and ATEM Switchers (via oscATEM).

Uses the [Artnet4j-Elios](https://github.com/Eliosoft/artnet4j-elios) library to listen to the ArtNet network. This library is a fork supporting DMX Input, based on [ArtNet4j](https://code.google.com/p/artnet4j/) by Karsten Schmidt (toxilibs).

## See also

* [Artnet4j installation](docs/artnet4j-installation.md) _Describes how to build Artnet4j JAR and configure Enttec's OpenDMX Ethernet_